# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.core.files.storage import FileSystemStorage
from django.db.models import Exists, OuterRef
from django.utils.translation import gettext_lazy as _



class Company(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    name = models.CharField(max_length=100)
    edited_name = models.CharField(max_length=100,null=True)
    caption = models.CharField(max_length=100,null=True)
    website = models.CharField(max_length=100,null=True)
    image = models.ImageField(upload_to='company/',null=True)
    summary = models.TextField(null=True)
    address = models.TextField(null=True)
    twitter_followers = models.IntegerField(default=0,null=True)
    instagram_followers = models.IntegerField(default=0,null=True)

    class Meta:
        ordering = ['creation_time']

    def __str__(self):
        return self.name


class Email(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='email')
    screenshot = models.ImageField(upload_to='emails/',null=True)
    # emails_tracked = models.IntegerField(default=0)

    class Meta:
        ordering = ['creation_time']


class FBPage(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name='fbpage')
    screenshot = models.ImageField(upload_to='fbpage/')
    fbpages_tracked = models.IntegerField(default=0)

    class Meta:
        ordering = ['creation_time']


class Page(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name='page')
    screenshot = models.ImageField(upload_to='page/')
    name = models.TextField()
    pages_tracked = models.IntegerField(default=0)

    class Meta:
        ordering = ['creation_time']


class Track(models.Model):
    creation_time = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    company = models.ForeignKey(
        Company, on_delete=models.CASCADE, related_name='company_tracked')
    user = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='user_tracked')

    class Meta:
        ordering = ['creation_time']



class MailGun(models.Model):
    recipient = models.TextField(null=True, blank=True)
    sender = models.TextField(null=True, blank=True)
    capital_from = models.TextField(null=True, blank=True)
    from_from = models.TextField(null=True, blank=True)
    subject = models.TextField(null=True, blank=True)
    subject_subject = models.TextField(null=True, blank=True)
    body_plain = models.TextField(null=True, blank=True)
    stripped_text = models.TextField(null=True, blank=True)
    stripped_signature = models.TextField(null=True, blank=True)
    body_html = models.TextField(null=True, blank=True)
    stripped_html = models.TextField(null=True, blank=True)
    attachment_count = models.IntegerField(null=True, blank=True)
    attachment_x = models.TextField(null=True, blank=True)
    timestamp = models.IntegerField(null=True, blank=True)
    token = models.TextField(null=True, blank=True)
    signature = models.TextField(null=True, blank=True)
    message_headers = models.TextField(null=True, blank=True)
    content_id_map = models.TextField(null=True, blank=True)
    domain = models.TextField(null=True, blank=True)
    x_envelope_from = models.TextField(null=True, blank=True)
    x_google_dkim_signature = models.TextField(null=True, blank=True)
    to_to = models.TextField(null=True, blank=True)
    dkim_signature = models.TextField(null=True, blank=True)
    x_received = models.TextField(null=True, blank=True)
    date_date = models.TextField(null=True, blank=True)
    message_id = models.TextField(null=True, blank=True)
    mime_version = models.TextField(null=True, blank=True)
    received_received = models.TextField(null=True, blank=True)
    message_url = models.TextField(null=True, blank=True)
    x_mailgun_incoming = models.TextField(null=True, blank=True)
    x_gm_message_state = models.TextField(null=True, blank=True)
    content_type = models.TextField(null=True, blank=True)
    x_google_smtp_source = models.TextField(null=True, blank=True)
    desktop_shot = models.ImageField(null=True, blank=True, upload_to='desktop')
    mobile_shot = models.ImageField(null=True, blank=True, upload_to='desktop')

    def __str__(self):
        return self.sender

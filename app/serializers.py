from django.contrib.auth.models import User
from rest_framework import serializers
from django.core import serializers as coreSerializer

from app.models import * 
from django.db.models import Q


class UserSerializer(serializers.ModelSerializer):

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance

    def update(self, instance, validated_data):
        for attr, value in validated_data.items():
            if attr == 'password':
                instance.set_password(value)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance
        
    class Meta:
        model = User
        fields = ('id', 'url','password', 'email','first_name','last_name','date_joined','username')
        

        
class CompanyCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Company
        fields = '__all__'

class CompanySerializer(serializers.ModelSerializer):
    
    is_tracked = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Company
        fields = '__all__'
        read_only_fields = ['is_tracked']
    
    def get_is_tracked(self,obj):
        
        return Track.objects.filter(Q(user=self.context['request'].user) & Q(company=obj)).exists()
        # return Track.objects.filter(user=self.context['request'].user, company=obj).exists()
    

class EmailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Email
        fields = ['id','company','screenshot']

class EmailReadSerializer(EmailSerializer):
    
    type = serializers.ReadOnlyField(default='Email')
    company = CompanySerializer(read_only=True)
    emails_tracked = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Email
        fields = ['id','company','screenshot','emails_tracked','type','creation_time']
    
    def get_emails_tracked(self,obj):
        
        return Email.objects.filter(company=obj.company).count()


class FBPageSerializer(serializers.ModelSerializer):

    class Meta:
        model = FBPage
        fields = ['id','company','screenshot','fbpages_tracked']

class FBPageReadSerializer(FBPageSerializer):

    type = serializers.ReadOnlyField(default='FbPage')
    company = CompanySerializer(read_only=True)

    class Meta:
        model = FBPage
        fields = ['id','company','screenshot','fbpages_tracked','type','creation_time']

class PageSerializer(serializers.ModelSerializer):

    class Meta:
        model = Page
        fields = ['id','company','screenshot','name','pages_tracked']

class PageReadSerializer(PageSerializer):

    type = serializers.ReadOnlyField(default='Page')
    company = CompanySerializer(read_only=True)

    class Meta:
        model = Page
        fields = ['id','company','screenshot','name','pages_tracked','type','creation_time']

class TrackSerializer(serializers.ModelSerializer):

    class Meta:
        model = Track
        fields = ['id','company','user']

class TrackReadSerializer(TrackSerializer):

    
    company = CompanySerializer(read_only=True)

class CompanyRetreiveSerialzier(serializers.ModelSerializer):
    
    emails = serializers.SerializerMethodField('get_email_data')
    fbpages = serializers.SerializerMethodField('get_fbpage_data')
    pages = serializers.SerializerMethodField('get_page_data')
    is_tracked = serializers.SerializerMethodField(read_only=True)

    def get_email_data(self, obj):
        company_id = self.context.get("pk")
        query_set =  Email.objects.filter(company__id=company_id)[:5]
        serializer = EmailReadSerializer(query_set,many=True,
        context={'request': self.context.get("request")})
        
        return serializer.data
    
    def get_fbpage_data(self, obj):
        company_id = self.context.get("pk")
        query_set =  FBPage.objects.filter(company__id=company_id)[:5].values()
        serializer = FBPageReadSerializer(query_set,many=True,
        context={'request': self.context.get("request")})

        return serializer.data
    
    def get_page_data(self, obj):
        company_id = self.context.get("pk")
        query_set = Page.objects.filter(company__id=company_id)[:5]
        serializer = PageReadSerializer(query_set,many=True,
        context={'request': self.context.get("request")})

        return serializer.data
        
    def get_is_tracked(self,obj):
        
        return Track.objects.filter(Q(user=self.context['request'].user) & Q(company=obj)).exists()

    
    class Meta:
        model = Company
        fields = ('id','name','caption','website','image','summary','address'
        ,'twitter_followers','instagram_followers','emails','fbpages','pages','is_tracked')

## Kami CODE

class MailGunSerializer(serializers.ModelSerializer):
    """
    MailGun
    """
    class Meta:
        model = MailGun
        fields = '__all__'

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Company)
admin.site.register(Email)
admin.site.register(FBPage)
admin.site.register(Page)
admin.site.register(Track)
admin.site.register(MailGun)
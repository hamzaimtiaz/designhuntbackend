"""

All basic views of features of restaurant other than its traits

"""

from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.db import connection
from django.core import serializers

from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.pagination import PageNumberPagination
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework import generics

from drf_multiple_model.views import ObjectMultipleModelAPIView
from drf_multiple_model.pagination import MultipleModelLimitOffsetPagination

import json
import imgkit
import io
import os
from PIL import Image
from django.core.files.uploadedfile import SimpleUploadedFile

# import cv2

import re
from app.serializers import *
from .serializers import UserSerializer
from django.db.models import Q


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """

    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

class CompanyNewViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    """
    A viewset that provides the standard actions
    """
    permission_classes = (IsAuthenticated,)
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    def list(self, request):
        queryset = Company.objects.filter(Q(name__contains="ruelala") | Q(name__contains="macys") | Q(name__contains="thehut"))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = CompanySerializer(
                page,
                many=True,
                context={'request': request}
            )
            return self.get_paginated_response(serializer.data)

        serializer = CompanySerializer(
            queryset,
            many=True,
            context={'request': request}
        )
        return Response(serializer.data, status=status.HTTP_200_OK)
class CompanyViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    """
    A viewset that provides the standard actions
    """
    permission_classes = (IsAuthenticated,)
    queryset = Company.objects.all()
    serializer_class = CompanySerializer

    def list(self, request):
        if(request.query_params):
            queryset = Company.objects.filter(
                name__contains=request.query_params['search'])
        else:
            queryset = Company.objects.all()

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = CompanySerializer(
                page,
                many=True,
                context={'request': request}
            )
            return self.get_paginated_response(serializer.data)

        serializer = CompanySerializer(
            queryset,
            many=True,
            context={'request': request}
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        serializer = CompanyCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        instance = get_object_or_404(Company, pk=pk)
        serializer = CompanyRetreiveSerialzier(instance,
                                               context={'request': request, 'pk': pk})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        instance = get_object_or_404(Company, pk=pk)
        serializer = CompanyCreateSerializer(
            instance=instance,
            data=request.data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        instance = get_object_or_404(Company, pk=pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class EmailViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    """
    A viewset that provides the standard actions
    """
    permission_classes = (IsAuthenticated,)
    queryset = Email.objects.all()
    serializer_class = EmailSerializer

    def get_queryset(self):
        self.companies = Track.objects.filter(
            user=self.request.user).distinct().values_list('company')
        queryset = Email.objects.filter(company__in=self.companies)
        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = EmailReadSerializer(
                page,
                many=True,
                context={'request': request}
            )
            return self.get_paginated_response(serializer.data)

        serializer = EmailReadSerializer(
            queryset,
            many=True,
            context={'request': request}
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        serializer = EmailSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        instance = get_object_or_404(Email, pk=pk)
        serializer = EmailReadSerializer(
            instance, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        instance = get_object_or_404(Email, pk=pk)
        serializer = EmailSerializer(
            instance=instance,
            data=request.data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        instance = get_object_or_404(Email, pk=pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class FBPageViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    """
    A viewset that provides the standard actions
    """
    permission_classes = (IsAuthenticated,)
    queryset = FBPage.objects.all()
    serializer_class = FBPageSerializer

    def get_queryset(self):
        self.companies = Track.objects.filter(
            user=self.request.user).distinct().values_list('company')
        queryset = FBPage.objects.filter(company__in=self.companies)
        return queryset

    def list(self, request):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = FBPageReadSerializer(
                page,
                many=True,
                context={'request': request}
            )
            return self.get_paginated_response(serializer.data)

        serializer = FBPageReadSerializer(
            queryset,
            many=True,
            context={'request': request}
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        serializer = FBPageSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        instance = get_object_or_404(FBPage, pk=pk)
        serializer = FBPageReadSerializer(
            instance, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        instance = get_object_or_404(FBPage, pk=pk)
        serializer = FBPageSerializer(
            instance=instance,
            data=request.data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        instance = get_object_or_404(FBPage, pk=pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class PageViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    """
    A viewset that provides the standard actions
    """
    permission_classes = (IsAuthenticated,)
    queryset = Page.objects.all()
    serializer_class = PageSerializer

    def get_queryset(self):
        self.companies = Track.objects.filter(
            user=self.request.user).distinct().values_list('company')
        queryset = Page.objects.filter(company__in=self.companies)
        return queryset

    def list(self, request):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = PageReadSerializer(
                page,
                many=True,
                context={'request': request}
            )
            return self.get_paginated_response(serializer.data)

        serializer = PageReadSerializer(
            queryset,
            many=True,
            context={'request': request}
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        serializer = PageSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        instance = get_object_or_404(Page, pk=pk)
        serializer = PageReadSerializer(instance, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        instance = get_object_or_404(Page, pk=pk)
        serializer = PageSerializer(
            instance=instance,
            data=request.data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        instance = get_object_or_404(Page, pk=pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class TrackViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    """
    A viewset that provides the standard actions
    """
    permission_classes = (IsAuthenticated,)
    queryset = Track.objects.all()
    serializer_class = TrackSerializer

    def get_queryset(self):

        queryset = Track.objects.filter(user_id=self.request.user)
        return queryset

    def list(self, request):
        queryset = Track.objects.filter(user_id=self.request.user)
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = TrackReadSerializer(
                page,
                many=True,
                context={'request': request}
            )
            return self.get_paginated_response(serializer.data)

        serializer = TrackReadSerializer(
            queryset,
            many=True,
            context={'request': request}
        )
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request):
        serializer = TrackSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None):
        instance = get_object_or_404(Track, pk=pk)
        serializer = TrackReadSerializer(
            instance, context={'request': request})
        return Response(serializer.data, status=status.HTTP_200_OK)

    def update(self, request, pk=None):
        instance = get_object_or_404(Track, pk=pk)
        serializer = TrackSerializer(
            instance=instance,
            data=request.data,
            partial=True
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None):
        instance = get_object_or_404(Track, pk=pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class MainPageView(APIView):
    """
    Returns the Details of the Main Page
    """

    def get(self, request, format=None, **kwargs):
        emails_serializer = EmailReadSerializer(EmailViewSet.get_queryset(self), many=True,
                                                context={'request': request})
        fb_page_serializer = FBPageReadSerializer(FBPageViewSet.get_queryset(self), many=True,
                                                  context={'request': request})
        page_serializer = PageReadSerializer(PageViewSet.get_queryset(self), many=True,
                                             context={'request': request})
        # tracked_company_serializer = TrackReadSerializer(TrackViewSet.get_queryset(self), many=True,
        #                                  context={'request': request})

        if(len(emails_serializer.data) > 10):
            emails = emails_serializer.data[-10:]
        else:
            emails = emails_serializer.data

        if(len(fb_page_serializer.data) > 10):
            fb_page = fb_page_serializer.data[-10:]
        else:
            fb_page = fb_page_serializer.data

        if(len(page_serializer.data) > 10):
            page = page_serializer.data[-10:]
        else:
            page = page_serializer.data

        return Response({
            'Email': emails,
            'FBPage': fb_page,
            'Page': page,
            # 'tracked_company':tracked_company_serializer.data[10]
        })


class LimitPagination(MultipleModelLimitOffsetPagination):
    default_limit = 12


class NewMainPageView(ObjectMultipleModelAPIView):
    pagination_class = LimitPagination

    def get_querylist(self):
        company = Track.objects.filter(
            user=self.request.user).distinct().values_list('company')

        querylist = [
            {'queryset': Email.objects.filter(
                company__in=company), 'serializer_class': EmailReadSerializer},
            {'queryset': FBPage.objects.filter(
                company__in=company), 'serializer_class': FBPageReadSerializer},
            {'queryset': FBPage.objects.filter(
                company__in=company), 'serializer_class': FBPageReadSerializer},
            {'queryset': Page.objects.filter(
                company__in=company), 'serializer_class': PageReadSerializer},
            {'queryset': Track.objects.filter(
                user_id=self.request.user), 'serializer_class': TrackReadSerializer},
        ]

        return querylist

# KAMI CODE


class MailGunViewSet(generics.ListCreateAPIView):
    """
    RestInterface.v2.views.OrderDoughnutViewSet
    """
    serializer_class = MailGunSerializer
    def html_topng(self, html):

        image = os.path.abspath(os.getcwd())+'/out.png'
        try:
            imgkit.from_string(html, image)
        except OSError:
            pass
        finally:
            with open(image, "rb") as fh:
                img = Image.open(fh, mode='r')

                imgByteArr = io.BytesIO()
                img.save(imgByteArr, format='PNG')
                byte = imgByteArr.getvalue()
            
            return byte
            
    def company_extractor2(self,data):

        if data:
            sub_words = data.split('@')[1].split('.')
            if (len(sub_words)<3):
                return (sub_words[0],sub_words[0]+'.'+sub_words[1].split('>')[0])
            if(len(sub_words)==3):
                return (sub_words[1],sub_words[1]+'.'+sub_words[2].split('>')[0])
        #     if(bool(re.search(r'\d', sub_words[2]))):
        #         return sub_words[1]
        
            return (sub_words[2],sub_words[2]+'.'+sub_words[3].split('>')[0])
        
        return None

    def company_extractor(self, data):
        sub_words = data.split('@')[1].split('.')
        if (len(sub_words) < 3):
            return (sub_words[0],None)
        if(len(sub_words) == 3):
            return (sub_words[1],None)
        if(bool(re.search(r'\d', sub_words[2]))):
            return (sub_words[1],None)

        return (sub_words[2],None)

    def get_queryset(self):
        hostname = self.request.query_params.get('hostname') or None
        if hostname:
            return MailGun.objects.filter(sender__contains=hostname)
        return []

    def perform_create(self, serializer):

        print("In Callback")
        instance = serializer.save()
        instance.capital_from = self.request.data.get('From', None)
        instance.from_from = self.request.data.get('from', None)
        instance.x_envelope_from = self.request.data.get(
            'X-Envelope-From', None)
        instance.subject_subject = self.request.data.get('Subject', None)
        # instance.x_google_dkim_signature = self.request.data.get('X-Google-Dkim-Signature', None)
        # instance.to_to = self.request.data.get('To', None)
        instance.dkim_signature = self.request.data.get('Dkim-Signature', None)
        instance.x_received = self.request.data.get('X-Received', None)
        instance.date_date = self.request.data.get('Date', None)
        instance.message_id = self.request.data.get('Message-Id', None)
        instance.mime_version = self.request.data.get('Mime-Version', None)
        # instance.received_received = self.request.data.get('Received', None)
        instance.message_url = self.request.data.get('message-url', None)
        instance.x_mailgun_incoming = self.request.data.get(
            'X-Mailgun-Incoming', None)
        # instance.x_gm_message_state = self.request.data.get('X-Gm-Message-State', None)
        instance.message_headers = self.request.data.get(
            'message-headers', None)
        instance.content_type = self.request.data.get('Content-Type', None)
        instance.x_google_smtp_source = self.request.data.get(
            'X-Google-Smtp-Source', None)
        instance.subject_subject = self.request.data.get('Subject', None)
        instance.body_plain = self.request.data.get('body-plain', None)
        instance.body_html = self.request.data.get('body-html', None)
        instance.stripped_text = self.request.data.get('stripped-text', None)
        instance.stripped_html = self.request.data.get('stripped-html', None)
        instance.save()  # author=self.request.user)

        company_name = self.company_extractor2(self.request.data.get('From', None))
        if(company_name is None):
            company_name = self.company_extractor(self.request.data['sender'])

        try:
            company = Company.objects.get(name=company_name[0])
            company_id = company.id

        except:
            data = {'name': company_name[0],'edited_name':company_name[0],'website':company_name[1]}
            company_serializer = CompanyCreateSerializer(data=data)
            company_serializer.is_valid(raise_exception=True)
            company_serializer.save()
            company = company_serializer.data
            company_id = company['id']

        if(self.request.data.get('body-html', None)):
            # try:
            pic = self.html_topng(self.request.data.get('body-html', None))
            if pic:
                data = {'company': company_id, 'screenshot': None}
                email_serializer = EmailSerializer(data=data)
                email_serializer.is_valid(raise_exception=True)
                email_instance = email_serializer.save()
                image_in_memory = SimpleUploadedFile("foo.png", pic, 'emails/')
                email_instance.screenshot.save(str(email_instance.id) + '.jpg',image_in_memory)
                email_instance.save()
            # except:
            #     print("Image not loaded")

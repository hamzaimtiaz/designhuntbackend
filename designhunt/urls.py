from django.urls import include, path
from rest_framework import routers
from django.contrib import admin
# from jet_django.urls import jet_urls
from app import views

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'company', views.CompanyViewSet)
router.register(r'email', views.EmailViewSet)
router.register(r'fb-page', views.FBPageViewSet)
router.register(r'page', views.PageViewSet)
router.register(r'tracked', views.TrackViewSet)
router.register(r'newsignup', views.CompanyNewViewSet)
# router.register(r'main', views.NewMainPageView,basename='main')


urlpatterns = [

    # path('jet/', include('jet.urls', 'jet')),
    path('grappelli/', include('grappelli.urls')),
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('api/main1/', views.MainPageView.as_view(), name='main'),
    path('api/main/', views.NewMainPageView.as_view(), name='main'),
    path('api/callback/', views.MailGunViewSet.as_view()),
    # path('api/newsignup/', views.CompanyNewViewSet),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

]
